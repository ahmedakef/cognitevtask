package database

import (
	"database/sql"
	"fmt"
	"time"

	// sqlite3 adaptor
	_ "github.com/mattn/go-sqlite3"
	log "github.com/sirupsen/logrus"
)

// DB is the handler of database connnection
type DB struct {
	conn *sql.DB
}

// Connect connect to the database
func (db *DB) Connect() error {
	var err error
	db.conn, err = sql.Open("sqlite3", "./pidsDB.db")
	if err == nil {
		log.Info("connection established with database successfully")
	}
	return err
}

// InsertPid inserts pidValue on per with id = petID
func (db DB) InsertPid(petID, pidValue int) {
	createdAt := time.Now()

	_, err := db.conn.Exec(`INSERT INTO pids (petID, pidValue, created_at) VALUES (?, ?, ?)`, petID, pidValue, createdAt)
	if err != nil {
		log.Fatal(err)
	}
}

// GetPids get the pids of pet with id = petID
func (db DB) GetPids(petID int) []int {
	query := fmt.Sprint("SELECT pidValue FROM pids WHERE petID = ", petID)
	rows, err := db.conn.Query(query)
	if err != nil {
		log.Fatal(err)
	}

	var pids []int
	var pidValue int

	for rows.Next() {
		err = rows.Scan(&pidValue)
		pids = append(pids, pidValue)

	}

	rows.Close()

	return pids
}

// Close closes the connection with database
func (db DB) Close() {
	db.conn.Close()
}
