package database

import (
	"database/sql"
	"os"

	// sqlite3 adaptor
	_ "github.com/mattn/go-sqlite3"
	log "github.com/sirupsen/logrus"
)

// CreateDB creates the database scheme
func CreateDB() error {
	os.Remove("./pidsDB.db")

	db, err := sql.Open("sqlite3", "./pidsDB.db")
	defer db.Close()

	creatQuery := `CREATE TABLE pids (
        uid INTEGER PRIMARY KEY AUTOINCREMENT,
        petID INTEGER,
        pidValue INTEGER,
        created_at DATE NULL
	);`
	_, err = db.Exec(creatQuery)
	if err == nil {
		log.Info("database created successfully")
	}

	return err

}
