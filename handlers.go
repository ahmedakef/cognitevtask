package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// Pid contains the pid value
type Pid struct {
	PidValue int
}

func bidOnPet(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	petID, _ := strconv.Atoi(vars["petId"])

	var pid Pid

	err := json.NewDecoder(r.Body).Decode(&pid)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	db.InsertPid(petID, pid.PidValue)
	fmt.Fprintf(w, "You have pid on pid: %d with pid value = %d\n", petID, pid.PidValue)
}

func getPids(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	petID, _ := strconv.Atoi(vars["petId"])

	var pids []int = db.GetPids(petID)
	json.NewEncoder(w).Encode(pids)
}
