module cognitevTask

go 1.13

require (
	github.com/gorilla/mux v1.7.4
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/sirupsen/logrus v1.6.0
	golang.org/x/sys v0.0.0-20200523222454-059865788121 // indirect
)
