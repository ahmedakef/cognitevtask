package main

import (
	"net/http"
	"os"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"cognitevTask/database"
)

var db database.DB

func main() {

	if len(os.Args) > 1 && os.Args[1] == "createDB" {
		err := database.CreateDB()
		if err != nil {
			log.Fatal(err)
		}
		return
	}
	r := mux.NewRouter()

	r.HandleFunc("/pet/{petId}/pid", bidOnPet).Methods("POST")
	r.HandleFunc("/pet/{petId}/getPids", getPids).Methods("GET")

	err := db.Connect()
	if err != nil {
		log.Fatal(err)
	}

	log.Info("Listening on Port : 8080")
	http.ListenAndServe(":8080", r)

	db.Close()
}
