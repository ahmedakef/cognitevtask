FROM golang:latest

# Move to working directory /app
WORKDIR /app

# Copy and download dependency using go mod
COPY go.mod .
COPY go.sum .
RUN go mod download

# Copy the code into the container
COPY ./ /app

RUN go get github.com/githubnemo/CompileDaemon

RUN go build

ENTRYPOINT ["CompileDaemon", "--command=./cognitevTask"]
