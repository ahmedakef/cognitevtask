# petstore pids

## using docker


```
# build the project
docker-compose build

# we use CompileDaemon to build the project if any file changed
sudo docker-compose up -d

# to create the database
sudo docker-compose exec web ./cognitevTask createDB
```


## create a pid using curl

```
curl -d '{"PidValue":500}' -H "Content-Type: application/json" -X POST http://localhost:8080/pet/30/pid
```


## get all pids for a single pet using curl

```
curl http://localhost:8080/pet/30/getPids
```